# coding: utf-8

# In[1]:

#%%
import os
import sys
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
# run for jupyter notebook
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'
#%%

# In[2]:

working = '/home/alal/Desktop/code/eq-svy-eda'
get_ipython().run_line_magic('cd', '$working')
raw = '/home/alal/Desktop/data/Nepal-eq-survey/Nepal-eq-NPC-full-survey'

# ## Admin identifiers merge - Run once

# In[3]:

root = '/home/alal/Desktop/data/Nepal-eq-survey/Nepal-eq-NPC-full-survey'
get_ipython().run_line_magic('cd', "$root'/crosswalks'")
get_ipython().run_line_magic('ls', '*.csv')

# In[4]:

samp_dists = [
    'dhading', 'gorkha', 'nuwakot', 'rasuwa', 'kavrepalanchok', 'okhaldhunga',
    'sindhuli', 'dolakha', 'makwanpur', 'ramechhap', 'sindhupalchok'
]
# In[5]:

dist = pd.read_csv('0_district-codes.csv')
dist['District_Name'] = dist['District_Name'].str.lower()

# *read in district-vdc dataframes and manually add column for district code for merge*
# tedious, but column editing helps

# In[6]:

dhading = pd.read_csv('dhading.csv')
dhading['District'] = 30
dolakha = pd.read_csv('dolakha.csv')
dolakha['District'] = 22
gorkha = pd.read_csv('gorkha.csv')
gorkha['District'] = 36
kavrepalanchok = pd.read_csv('kavrepalanchok.csv')
kavrepalanchok['District'] = 24
makwanpur = pd.read_csv('makwanpur.csv')
makwanpur['District'] = 31
nuwakot = pd.read_csv('nuwakot.csv')
nuwakot['District'] = 28
okhaldhunga = pd.read_csv('okhaldhunga.csv')
okhaldhunga['District'] = 12
ramechhap = pd.read_csv('ramechhap.csv')
ramechhap['District'] = 21
rasuwa = pd.read_csv('rasuwa.csv')
rasuwa['District'] = 29
sindhuli = pd.read_csv('sindhuli.csv')
sindhuli['District'] = 20
sindhupalchok = pd.read_csv('sindhupalchok.csv')
sindhupalchok['District'] = 23

kathmandu = pd.read_csv('kathmandu.csv')
kathmandu['District'] = 27
lalitpur = pd.read_csv('lalitpur.csv')
lalitpur['District'] = 25
bhaktapur = pd.read_csv('bhaktapur.csv')
bhaktapur['District'] = 26
tanahu = pd.read_csv('tanahu.csv')
tanahu['District'] = 38
lamjung = pd.read_csv('lamjung.csv')
lamjung['District'] = 37
solukhumbu = pd.read_csv('solukhumbu.csv')
solukhumbu['District'] = 11
syangja = pd.read_csv('syangja.csv')
syangja['District'] = 39
chitwan = pd.read_csv('chitwan.csv')
chitwan['District'] = 35
khotang = pd.read_csv('khotang.csv')
khotang['District'] = 13
parbat = pd.read_csv('parbat.csv')
parbat['District'] = 44
kaski = pd.read_csv('kaski.csv')
kaski['District'] = 40
palpa = pd.read_csv('palpa.csv')
palpa['District'] = 47
bhojpur = pd.read_csv('bhojpur.csv')
bhojpur['District'] = 10
gulmi = pd.read_csv('gulmi.csv')
gulmi['District'] = 46
sankhuwasabha = pd.read_csv('sankhuwasabha.csv')
sankhuwasabha['District'] = 9
dhankuta = pd.read_csv('dhankuta.csv')
dhankuta['District'] = 7
baglung = pd.read_csv('baglung.csv')
baglung['District'] = 45
arghakhanchi = pd.read_csv('arghakhanchi.csv')
arghakhanchi['District'] = 51
myagdi = pd.read_csv('myagdi.csv')
myagdi['District'] = 43
nawalparasi = pd.read_csv('nawalparasi.csv')
nawalparasi['District'] = 48

# In[7]:

dhading = dhading.merge(dist, on='District')
dolakha = dolakha.merge(dist, on='District')
gorkha = gorkha.merge(dist, on='District')
kavrepalanchok = kavrepalanchok.merge(dist, on='District')
makwanpur = makwanpur.merge(dist, on='District')
nuwakot = nuwakot.merge(dist, on='District')
okhaldhunga = okhaldhunga.merge(dist, on='District')
ramechhap = ramechhap.merge(dist, on='District')
rasuwa = rasuwa.merge(dist, on='District')
sindhuli = sindhuli.merge(dist, on='District')
sindhupalchok = sindhupalchok.merge(dist, on='District')

kathmandu = kathmandu.merge(dist, on='District')
lalitpur = lalitpur.merge(dist, on='District')
bhaktapur = bhaktapur.merge(dist, on='District')
tanahu = tanahu.merge(dist, on='District')
lamjung = lamjung.merge(dist, on='District')
solukhumbu = solukhumbu.merge(dist, on='District')
syangja = syangja.merge(dist, on='District')
chitwan = chitwan.merge(dist, on='District')
khotang = khotang.merge(dist, on='District')
parbat = parbat.merge(dist, on='District')
kaski = kaski.merge(dist, on='District')
palpa = palpa.merge(dist, on='District')
bhojpur = bhojpur.merge(dist, on='District')
gulmi = gulmi.merge(dist, on='District')
sankhuwasabha = sankhuwasabha.merge(dist, on='District')
dhankuta = dhankuta.merge(dist, on='District')
baglung = baglung.merge(dist, on='District')
arghakhanchi = arghakhanchi.merge(dist, on='District')
myagdi = myagdi.merge(dist, on='District')
nawalparasi = nawalparasi.merge(dist, on='District')

# In[8]:

DistDfList = [
    dhading, dolakha, gorkha, kavrepalanchok, makwanpur, nuwakot, okhaldhunga,
    ramechhap, rasuwa, sindhuli, sindhupalchok, kathmandu, lalitpur, bhaktapur,
    tanahu, lamjung, solukhumbu, syangja, chitwan, khotang, parbat, kaski,
    palpa, bhojpur, gulmi, sankhuwasabha, dhankuta, baglung, arghakhanchi,
    myagdi, nawalparasi
]

# In[9]:

stackedXwalk = pd.concat(DistDfList).reset_index(drop=True).sort_values(
    by='District')
stackedXwalk['muncode'] = stackedXwalk['VDC-Municipality-Code'].map(str).apply(
    lambda x: x.zfill(2))
stackedXwalk.head()

# In[10]:

stackedXwalk[
    'vdcmun_id'] = stackedXwalk['District'].map(str) + stackedXwalk['muncode']
stackedXwalk.vdcmun_id = stackedXwalk.vdcmun_id.astype(np.int64)
# f.column_name = df.column_name.astype(np.int64)

# In[11]:

stackedXwalk.drop(['muncode'], axis=1, inplace=True)

# In[12]:

# stackedXwalk['FullSample'] = np.where(stackedXwalk['District_Name'] in samp_dists, 1, 0)
stackedXwalk['FullSample'] = np.where(
    stackedXwalk['District_Name'].isin(samp_dists), 1, 0)

# In[13]:

stackedXwalk.head()

# In[14]:

stackedXwalk.info()

# In[15]:

stackedXwalk.to_csv('00_all_xwalk.csv', index=False)

# ## Merge - run once

# In[16]:

root = '/home/alal/Desktop/data/Nepal-eq-survey/Nepal-eq-NPC-full-survey'
get_ipython().run_line_magic('cd', '$root')
get_ipython().run_line_magic('ls', '')

# In[17]:

hh_dem = pd.read_csv('csv_household_demographics.csv')
hh_res = pd.read_csv('csv_household_resources.csv')
hh_eqk = pd.read_csv('csv_household_earthquake_impact.csv')

# In[18]:

hh_eqk.info()

# In[19]:

hh_dem.info()

# In[20]:

hh_res.info()

# In[21]:

get_ipython().run_line_magic(
    'time',
    "hh_all = pd.merge(hh_dem,pd.merge(hh_eqk,hh_res,on='household_id'),on='household_id')"
)

# In[22]:

hh_all.shape

# In[23]:

hh_all.info()

# In[24]:

hh_all.drop(
    [
        'id_x', 'district_id_x', 'vdcmun_id_x', 'ward_id_x', 'id_x',
        'district_id_y', 'vdcmun_id_y', 'ward_id_y', 'id_y'
    ],
    axis=1,
    inplace=True)

# In[25]:

hh_all.head()

# In[26]:

get_ipython().run_line_magic(
    'time', "hh_all.to_csv(working+'/data/interim/hh_merged.csv',index=False)")

# ## Merge geo with household data

# In[27]:

get_ipython().run_line_magic(
    'time', "hh_all = pd.read_csv(working+'/data/interim/hh_merged.csv')")
stackedXwalk = pd.read_csv(raw + '/crosswalks/00_all_xwalk.csv')

# In[28]:

hh_all.info()

# In[29]:

hh_all = hh_all.merge(stackedXwalk, on='vdcmun_id', how='left')

# In[33]:

cols = hh_all.columns.tolist()
geotags = [
    'VDC-Municipality-Code', 'VDC-Municipality-Name', 'District',
    'District_Name', 'FullSample'
]

cols = cols[-5:] + cols[:-5]
hh_all = hh_all[cols]

# In[34]:

hh_all.head()

# In[35]:

hh_all.to_csv(working + '/data/interim/hh_with_geo.csv', index=False)
