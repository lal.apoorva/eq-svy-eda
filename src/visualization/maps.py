# coding: utf-8

# In[1]:

# get_ipython().run_line_magic('matplotlib', 'inline')

import seaborn as sns
import pandas as pd
import pysal as ps
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt

from scipy.stats import mstats
import statsmodels.api as sm
import statsmodels.formula.api as smf
# %matplotlib inline
# run for jupyter notebook
from IPython.core.debugger import Tracer
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'
#%%

# ## Shapefiles

# In[2]:

root = '/home/alal/Desktop/data/Boundaries/Nepal_Admin_Boundaries'
get_ipython().run_line_magic('cd', '$root')
# l1 = gpd.read_file('Regions Level 1.shp')
# l2 = gpd.read_file('Zones Level 2.shp')
# l5 = gpd.read_file('Wards Level 5.shp')
l3 = gpd.read_file('Districts Level 3.shp')
l4 = gpd.read_file('Village Development Committees Level 4.shp')

# ## Household Data

# In[3]:

working = '/home/alal/Desktop/code/eq-svy-eda'
maproot = '/home/alal/Desktop/code/eq-svy-eda/reports/maps'
get_ipython().run_line_magic('cd', '$working')
get_ipython().run_line_magic(
    'time', "hh_all = pd.read_csv(working+'/data/interim/hh_with_geo.csv')")

# In[4]:

hh_all['vdc'] = hh_all['VDC-Municipality-Name'].str.lower()

# In[5]:

main_dists = hh_all.loc[hh_all.FullSample == 1].District_Name.unique()
main_dists

# ### Subset and Merge - District Level

# In[6]:

l3['District'] = l3['DISTRICT'].str.lower()

# In[7]:

remaps = {
    "makawanpur": "makwanpur",
    "kabhrepalanchok": "kavrepalanchok",
    "chitawan": "chitwan"
}
l3['District'].replace(remaps, inplace=True)
main_dists_shp = l3.loc[l3['District'].isin(main_dists)]

# In[8]:

main_dists_eq = hh_all[hh_all['District_Name'].isin(main_dists)]

# In[9]:

# set income to lower-bound of each bin
incomes = {
    'Rs. 10 thousand': 5000,
    'Rs. 10-20 thousand': 10000,
    'Rs. 20-30 thousand': 20000,
    'Rs. 30-50 thousand': 30000,
    'Rs. 50 thousand or more': 50000
}
main_dists_eq['income'] = main_dists_eq['income_level_household'].map(incomes)

# ### Functions to groupby and plot

# In[10]:


def aggregate_indicator(var, oper='sum'):
    dist_lev_tallies = main_dists_eq.groupby('District_Name').agg({
        'household_id':
        'count',
        var:
        oper
    })
    dist_lev_tallies.reset_index(inplace=True)
    dist_lev_tallies.rename(
        columns={'household_id': 'number_of_households'}, inplace=True)
    if oper == 'sum':
        dist_lev_tallies['share_with_' + var] = (
            dist_lev_tallies[var] / dist_lev_tallies.number_of_households
        ) * 100
    return dist_lev_tallies


# In[11]:


def plot_variable(df, varn, save=False, label=''):
    merged_dist = main_dists_shp.merge(
        df, left_on='District', right_on='District_Name')
    merged_dist['coords'] = merged_dist['geometry'].apply(
        lambda x: x.representative_point().coords[:])
    merged_dist['coords'] = [coords[0] for coords in merged_dist['coords']]
    f, ax = plt.subplots(1, dpi=300)
    merged_dist.plot(
        column=varn, ax=ax, cmap=plt.cm.viridis, legend=True, linewidth=5)
    for idx, row in merged_dist.iterrows():
        plt.annotate(
            s=row['District'],
            xy=row['coords'],
            horizontalalignment='center',
            size=5)
    ax.set_axis_off()
    if label == '':
        label = varn
    f.suptitle(label)
    if save:
        plt.savefig('{0}_plot.png'.format(varn), dpi=300)
        plt.clf()


# In[12]:

v = 'has_death_occurred_last_12_months'
plot_variable(aggregate_indicator(v), 'share_with_' + v)

# In[13]:

v = 'is_recipient_rahat_10k'
plot_variable(aggregate_indicator(v), 'share_with_' + v)

# In[14]:

v = 'income'
aggregate_indicator(v, oper='mean')
plot_variable(aggregate_indicator(v, oper='mean'), v, label='income - mean')

# In[15]:

v = 'income'
aggregate_indicator(v, oper='std')
plot_variable(
    aggregate_indicator(v, oper='std'), v, label='income - Standard Deviation')

# In[16]:

get_ipython().run_line_magic('cd', '$maproot')

# In[17]:

get_ipython().system(" mkdir 'district_level' -p")

# In[18]:

get_ipython().run_line_magic('cd', "'district_level'")

# In[19]:

list_of_cols = [
    'is_bank_account_present_in_household',
    'has_death_occurred_last_12_months',
    'has_injury_loss_occurred_last_12_months',
    'has_education_drop_occurred_last_12_months',
    'has_pregnancy_treatment_drop_occurred_last_12_months',
    'has_vaccination_drop_occurred_last_12_months',
    'has_occupation_change_occurred_last_12_months',
    'count_occupation_change_last_12_months', 'is_recipient_rahat_15k',
    'is_recipient_rahat_10k', 'is_recipient_rahat_200k',
    'is_recipient_rahat_social_security_3k', 'is_recipient_rahat_none',
    'is_ineligible_rahat', 'has_asset_land_pre_eq', 'has_asset_tv_pre_eq',
    'has_asset_cable_pre_eq', 'has_asset_computer_pre_eq',
    'has_asset_internet_pre_eq', 'has_asset_telephone_pre_eq',
    'has_asset_mobile_phone_pre_eq', 'has_asset_fridge_pre_eq',
    'has_asset_motorcycle_pre_eq', 'has_asset_four_wheeler_family_use_pre_eq',
    'has_asset_four_wheeler_commercial_use_pre_eq', 'has_asset_none_pre_eq',
    'has_asset_land_post_eq', 'has_asset_tv_post_eq',
    'has_asset_cable_post_eq', 'has_asset_computer_post_eq',
    'has_asset_internet_post_eq', 'has_asset_telephone_post_eq',
    'has_asset_mobile_phone_post_eq', 'has_asset_fridge_post_eq',
    'has_asset_motorcycle_post_eq',
    'has_asset_four_wheeler_family_use_post_eq',
    'has_asset_four_wheeler_commercial_post_eq', 'has_asset_none_post_eq'
]

# In[20]:

get_ipython().run_cell_magic(
    'capture', '',
    "for v in list_of_cols:\n    plot_variable(aggregate_indicator(v),'share_with_'+v,True)"
)

# ## Merge Village level

# In[21]:

dist_remaps = {
    "makawanpur": "makwanpur",
    "kabhrepalanchok": "kavrepalanchok",
    "chitawan": "chitwan"
}

# In[22]:

l4['District'] = l4['DIST_NAME'].str.lower()
l4['Vdc'] = l4['VDC_NAME'].str.lower()
l3['District'].replace(dist_remaps, inplace=True)
main_dists_shp = l4.loc[l4['District'].isin(main_dists)]
main_dists_shp.shape

# #### remaps for village names

# In[23]:

remaps = {
    "aaru arbang": "aaruaarbad",
    "aaru chanaute": "aaruaarbad",
    "aarupokhari": "aaruaarbad",
    "agara": "agra",
    "alampu": "alambu",
    "ambote": "shikhar ambote",
    "atarpur": "attarpur",
    "badegau": "bandegaun",
    "bageswori": "bageshwari",
    "baksa": "baksha",
    "baldthali": "balthali",
    "balting": "walting",
    "bamti bhandar": "baluwa pati naldhun",
    "baramchi": "baramchae",
    "barpak": "warpak",
    "baruneshwor": "barudeshwor",
    "bhaise": "bhainse",
    "bharta pundyadevi": "bhartapunyadevi",
    "bhedapu": "bhedpu",
    "bhimeshwor municipality": "bhimeswor municipality",
    "bhimfedi": "bhimphedi",
    "bhotasipa": "bhotsipa",
    "bhote namlang": "bhotenamlang",
    "bhujee": "bhuji",
    "bhumesthan": "bhumisthan",
    "bhusafeda": "bhusaphedi",
    "bhussinga": "bhushanga",
    "bitijor bagaiya": "bitijor bagaincha",
    "bocha": "boch",
    "bolde fediche": "boldephadiche",
    "bridhim": "briddim",
    "chalal ganeshsthan": "chalalganeshsthan",
    "chankhu": "changkhu",
    "chaughada": "chaughoda",
    "chautara n.p.": "chautara",
    "chhatredeurali": "chhatre deurali",
    "chilankha": "chilangkha",
    "choubas": "chaubas",
    "chumchet": "chunchet",
    "chyasing kharka": "chyasingkharka",
    "bhadrutar": "bhadratar",
    "baluwapati deupur": "baluwadeubhumi",
    "bekhsimle ghartigaun": "bekhsimle ghartigaon",
    "banakhu chor": "wanakhu",
    "bhumlichok": "mumlichok",
    "dadiguranshe": "dandiguranse",
    "dandagaun/dandaghar": "dandagaun",
    "dandagoun": "danda gaun",
    "dapcha kasikhanda n.p.": "dapcha chatraebangha",
    "dhola": "dhol",
    "dhunkharka": "dhungkharka bahrabisae",
    "dudbhanjyang": "dudhbhanjyang",
    "dhussa": "dhursa",
    "dudhouli n.p.": "dudhauli",
    "dhuseni siwalaya": "ghusenisiwalaye",
    "dubachour": "dubachaur",
    "dhuskun": "gunsakun",
    "fakhel": "phakhel",
    "farpu": "tharpu",
    "falate bhumlu": "phalete",
    "falemetar": "phalametar",
    "faparbari": "phaparbari",
    "fasku": "phasku",
    "fediguth": "phedighooth",
    "fikuri": "phikuri",
    "foksingtar": "phoksingtar",
    "fujel": "phujel",
    "fulbari": "phulbari",
    "fulpingdanda": "phulchodanda",
    "fulpingkatti": "phulpingkatti",
    "fulpingkot": "phulpingkot",
    "gairi bisouna deupur": "gairi bisauna deupur",
    "ganeshthan": "ganeshsthan",
    "gankhu": "gangkhu",
    "gauri sankar": "gaurishankar",
    "ghyang sukathokar": "ghyangsukathokar",
    "gloche": "golche",
    "gogane": "gomane",
    "goswara": "goshwara",
    "gothpani": "gotpani",
    "gunsakot": "ghunsakot",
    "gunsi bhadaure": "gunsi",
    "gupteshwor": "gupteshwar",
    "halde kalika": "kalika halldae",
    "hariharpur gadhi": "hariharpurgadhi",
    "jalkanya": "jalkanyachapauli",
    "jeewanpur": "jiwanpur",
    "jhyaku": "jhyanku",
    "jiri n.p.": "jiri",
    "jyamrung": "jyamere",
    "kabhre": "kabre",
    "kalati bhumidanda": "kapali bhumaedanda",
    "kalinchowk": "kalinchok",
    "kalpabrishykha": "kalpabrikshya",
    "kamalamai municipality": "kamalami municipality",
    "kankada": "kangkada",
    "katike deurali": "kattike deurali",
    "katunje besi": "katunjebesi",
    "kerauja": "keroja",
    "kewalpur": "kebalpur",
    "khadag bhanjyang": "khadga bhanjyang",
    "khahare pangu": "khaharepangu",
    "khang sang": "khangsang",
    "khiji chandeshwori": "khijichandeshwori",
    "khigikati": "khijikaanthi",
    "khijifalate": "khijiflate",
    "kimtang": "kintang",
    "kiwool": "kiul",
    "kogate": "kagate",
    "kolati bhumlu": "kolanti",
    "kubukasthali": "kunbhukasthali",
    "kuibhir": "kuebhire",
    "kuseswor dumja": "kusheshwar dumja",
    "kyaneshwor": "kyaneshwar",
    "laharepouwa": "laharepauwa",
    "langarche": "lagarche",
    "lisankhu": "lisangkhu",
    "listikot": "listokot",
    "madan kundari": "madankundari",
    "madhavpur": "madhabpur",
    "mahadevdada": "mahadevdanda",
    "mahankal": "mahangkal",
    "mahankal chaur": "mahangkal",
    "mahendra jhyadi": "mahendrajhyadi",
    "mahendra jyoti": "mahendrajyoti bansdol",
    "majhi feda": "majhipheda",
    "makadum": "makadhum",
    "makwanpurgadhi": "makawanpur gadhi",
    "maneswnara": "maneswar",
    "mankha": "mangkha",
    "melamchi n.p.": "melamchi",
    "muchhok": "muchchok",
    "nagre gagarche": "nangregagarche",
    "namdu": "namdru",
    "mirkot": "virkot",
    "narmedeshwor": "narmadeshwor",
    "nayagaun deupur": "naya gaun deupur",
    "ramechhap municipality": "ramechhap",
    "manthali municipality": "ramechhap",
    "nilkantha n.p.": "nilkanth",
    "orang": "worang",
    "pagretar": "pangretar",
    "palte": "patale",
    "palungtar municipality": "palungtar",
    "patalekhet": "phalete",
    "panchkhal n.p.": "panchkhal",
    "petaku": "pedku",
    "pinkhuri": "phikuri",
    "pritee": "priti",
    "purano jhangajholi": "puranojhangajholi",
    "raniban": "ranibaan",
    "ratanchura": "ratanchur",
    "ravi opi": "rabiopi",
    "rawadolu": "rawadol",
    "ree gaun": "rigaun",
    "ryale": "ryale bihawar",
    "sahare": "shahare",
    "sailungeswor": "sailungeshwar",
    "salle bhumlu": "salle blullu",
    "salmechakala (taldhunga)": "salme taldhunga",
    "salyantar": "salyan tar",
    "samundradevi": "samudradevi kholegaun",
    "samundratar": "samudratar",
    "sanghutar": "sandhutar",
    "sankhupati chour": "sangkhupatichaur",
    "sanowangthali": "sanuwangthali",
    "santeswori (rampur)": "shanteshwari rampur",
    "sarada batase": "sharada (batase)",
    "sarasyunkhark": "sarsyunkharka",
    "sarikhet palase": "sarikhetpalase",
    "saurpani": "sairpani",
    "semjong": "semdhung",
    "serna": "sherma",
    "shreechaur": "shrichaor",
    "shreenathkot": "shrithankot",
    "shreepur chhatiwan": "shripur chhatiwan",
    "simalchour syampati": "syampati simalchaur",
    "sipa pokhare": "sipapokharae",
    "sipal kavre": "sinpal kavre",
    "sirthouli": "sirthauli",
    "sunam pokhari": "sumnampokhari",
    "syafru": "syaphru",
    "solpathana": "swalpathana",
    "sidhdhicharan n.p.": "shishneri",
    "sikhar ambote": "shikhar ambote",
    "sisneri mahadevsthan": "shishneri",
    "syaule bazar": "syaule",
    "takukot": "tarkukot",
    "takumajh lakuribot": "takumaj hlakuri",
    "taluwa": "tuluwa",
    "thampal chhap": "thumpakhar",
    "thangpalkot": "thapalkot",
    "thumi": "thumo",
    "tistung deurali": "tistung",
    "tokarpur": "thokarpur",
    "toksel": "tokshel",
    "tosramkhola": "tosarangkhola",
    "tukucha nala": "tukuchanala",
    "ugrachandi nala": "ugrachandinala",
    "yamunadanda": "yamuna danda",
    "yasam": "yesham",
    "thulachhap": "thulachaap",
    "thulogoun": "thulo gaun",
    "thum pakhar": "thumpakhar",
    "dhaibung": "jibjibe (nilkantha)",
    "dhumthang": "sindhukot",
    "dhunwakot": "ghunsakot",
    "gorkha municipality": "prithbinarayan municipality",
    "majuwa": "makadhum",
    "pokhari chauri": "chauri pokhari",
    "tamchet dudhpokhari": "dodhapokhari",
    "thaha n.p.": "bhimphedi"
}

# In[24]:

main_dists_eq['vdc'].replace(remaps, inplace=True)

# Aggregate statistics to village level

# In[25]:


def village_agg_indicator(var):
    vdc_lev_tallies = main_dists_eq.groupby('vdc').agg({
        'household_id':
        'count',
        var:
        np.sum,
        'District_Name':
        'first'
    })
    vdc_lev_tallies.reset_index(inplace=True)
    vdc_lev_tallies.rename(
        columns={'household_id': 'number_of_households'}, inplace=True)
    vdc_lev_tallies['share_with_' + var] = (
        vdc_lev_tallies[var] / vdc_lev_tallies.number_of_households) * 100
    return vdc_lev_tallies


# In[26]:


def plot_var_village(df, v, dist=' ', save=False, winsor=False, ub=0.99):
    # subset shapefile and sumstats file if dist is specified
    if dist != ' ':
        df1 = main_dists_shp.loc[main_dists_shp['District'] == dist]
        df2 = df.loc[df['District_Name'] == dist]
    else:
        df1 = main_dists_shp.copy()
        df2 = df.copy()
    # merge with shapefile
    merged_village = df1.merge(df2, left_on='Vdc', right_on='vdc', how='left')
    varn = 'share_with_' + v
    # fill missing cells with mean (not all villages are in census)
    merged_village[varn] = merged_village[varn].fillna(
        merged_village[varn].mean())
    # winsorize
    if winsor:
        ubound = df[varn].quantile(ub)
        merged_village.loc[merged_village[varn] > ubound, varn] = ubound
    var = varn
    # plot
    f, ax = plt.subplots(1, dpi=300)
    merged_village.plot(
        column=var, ax=ax, cmap=plt.cm.viridis, legend=True, linewidth=5)
    ax.set_axis_off()
    if dist != ' ':
        f.suptitle(varn + '\n' + dist)
    else:
        f.suptitle(varn)
    if save:
        plt.savefig('{0}_plot.png'.format(varn), dpi=300)
        plt.close()


# In[27]:

v = 'is_recipient_rahat_10k'
df = village_agg_indicator(v)
plot_var_village(df, v, winsor=True)

# In[28]:

plot_var_village(df, v, dist='gorkha', winsor=True)

# In[29]:

v = 'has_death_occurred_last_12_months'
df = village_agg_indicator(v)
# df = df[df.vdc != 'langtang'] # drop outlier
plot_var_village(df, v, winsor=True)

# In[38]:

v = 'has_death_occurred_last_12_months'
df = village_agg_indicator(v)
plot_var_village(df, v, winsor=True, ub=0.98)  # different (lower) upper bound

# In[31]:

list_of_cols = [
    'is_bank_account_present_in_household',
    'has_injury_loss_occurred_last_12_months',
    'has_death_occurred_last_12_months',
    'has_education_drop_occurred_last_12_months',
    'has_pregnancy_treatment_drop_occurred_last_12_months',
    'has_vaccination_drop_occurred_last_12_months',
    'has_occupation_change_occurred_last_12_months',
    'count_occupation_change_last_12_months', 'is_recipient_rahat_15k',
    'is_recipient_rahat_10k', 'is_recipient_rahat_200k',
    'is_recipient_rahat_social_security_3k', 'is_recipient_rahat_none',
    'is_ineligible_rahat', 'has_asset_land_pre_eq', 'has_asset_tv_pre_eq',
    'has_asset_cable_pre_eq', 'has_asset_computer_pre_eq',
    'has_asset_internet_pre_eq', 'has_asset_telephone_pre_eq',
    'has_asset_mobile_phone_pre_eq', 'has_asset_fridge_pre_eq',
    'has_asset_motorcycle_pre_eq', 'has_asset_four_wheeler_family_use_pre_eq',
    'has_asset_four_wheeler_commercial_use_pre_eq', 'has_asset_none_pre_eq',
    'has_asset_land_post_eq', 'has_asset_tv_post_eq',
    'has_asset_cable_post_eq', 'has_asset_computer_post_eq',
    'has_asset_internet_post_eq', 'has_asset_telephone_post_eq',
    'has_asset_mobile_phone_post_eq', 'has_asset_fridge_post_eq',
    'has_asset_motorcycle_post_eq',
    'has_asset_four_wheeler_family_use_post_eq',
    'has_asset_four_wheeler_commercial_post_eq', 'has_asset_none_post_eq'
]

# In[32]:

get_ipython().run_line_magic('cd', '$maproot')

# In[33]:

get_ipython().system(" mkdir 'village_level' -p")

# In[34]:

get_ipython().run_line_magic('cd', "'village_level'")

# In[35]:

get_ipython().run_cell_magic(
    'capture', '',
    'for v in list_of_cols:\n    plot_var_village(village_agg_indicator(v),\n                     v,winsor=True,save=True)'
)

# ## Village level by district

# In[36]:

village_root = maproot + '/village_level'

# In[37]:

get_ipython().run_cell_magic(
    'time', '',
    "%%capture\nfor d in main_dists:\n    os.chdir(village_root)\n    dist_folder = village_root+'/'+d\n    if not os.path.exists(dist_folder):\n        os.makedirs(dist_folder)\n    os.chdir(dist_folder)\n    for v in list_of_cols:\n        plot_var_village(village_agg_indicator(v),v,\n                         dist=d,winsor=True,save=True)"
)
